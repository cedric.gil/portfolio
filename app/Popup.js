var overlayWindowsServer = document.getElementById('overlay_windows_server')
var popupWindowsServer = document.getElementById('popup_windows_server')
var btnPopupWindowsServer = document.getElementById('windows_server')
var noClose = false

btnPopupWindowsServer.addEventListener('click', openPopupWindowsServer)
popupWindowsServer.addEventListener('click', noCloseWindowsServer)
overlayWindowsServer.addEventListener('click', closePopupWindowsServer)

function openPopupWindowsServer() {
    console.log('open')
    overlayWindowsServer.style.display = 'flex'
}
function noCloseWindowsServer() {
    noClose = true
}
function closePopupWindowsServer() {
    if(noClose === true) {
        noClose = false
    } else {
        overlayWindowsServer.style.display = 'none'
    }
}


var overlayLinux = document.getElementById('overlay_linux')
var popupLinux = document.getElementById('popup_linux')
var btnPopupLinux = document.getElementById('linux')

btnPopupLinux.addEventListener('click', openPopupLinux)
popupLinux.addEventListener('click', noCloseLinux)
overlayLinux.addEventListener('click', closePopupLinux)

function openPopupLinux() {
    console.log('open')
    overlayLinux.style.display = 'flex'
}
function noCloseLinux() {
    noClose = true
}
function closePopupLinux() {
    if(noClose === true) {
        noClose = false
    } else {
        overlayLinux.style.display = 'none'
    }
}


var overlayConfiguration = document.getElementById('overlay_configuration')
var popupConfiguration = document.getElementById('popup_configuration')
var btnPopupConfiguration = document.getElementById('configuration')

btnPopupConfiguration.addEventListener('click', openPopupConfiguration)
popupConfiguration.addEventListener('click', noCloseConfiguration)
overlayConfiguration.addEventListener('click', closePopupConfiguration)

function openPopupConfiguration() {
    console.log('open')
    overlayConfiguration.style.display = 'flex'
}
function noCloseConfiguration() {
    noClose = true
}
function closePopupConfiguration() {
    if(noClose === true) {
        noClose = false
    } else {
        overlayConfiguration.style.display = 'none'
    }
}


var overlayVirtualisation = document.getElementById('overlay_virtualisation')
var popupVirtualisation = document.getElementById('popup_virtualisation')
var btnPopupVirtualisation = document.getElementById('virtualisation')

btnPopupVirtualisation.addEventListener('click', openPopupVirtualisation)
popupVirtualisation.addEventListener('click', noCloseVirtualisation)
overlayVirtualisation.addEventListener('click', closePopupVirtualisation)

function openPopupVirtualisation() {
    console.log('open')
    overlayVirtualisation.style.display = 'flex'
}
function noCloseVirtualisation() {
    noClose = true
}
function closePopupVirtualisation() {
    if(noClose === true) {
        noClose = false
    } else {
        overlayVirtualisation.style.display = 'none'
    }
}


var overlayMyServ = document.getElementById('overlay_myserv')
var popupMyServ = document.getElementById('popup_myserv')
var btnPopupMyServ = document.getElementById('myserv')

btnPopupMyServ.addEventListener('click', openPopupMyServ)
popupMyServ.addEventListener('click', noCloseMyServ)
overlayMyServ.addEventListener('click', closePopupMyServ)

function openPopupMyServ() {
    console.log('open')
    overlayMyServ.style.display = 'flex'
}
function noCloseMyServ() {
    noClose = true
}
function closePopupMyServ() {
    if(noClose === true) {
        noClose = false
    } else {
        overlayMyServ.style.display = 'none'
    }
}


var overlayMonParc = document.getElementById('overlay_monparc')
var popupMonParc = document.getElementById('popup_monparc')
var btnPopupMonParc = document.getElementById('monparc')

btnPopupMonParc.addEventListener('click', openPopupMonParc)
popupMonParc.addEventListener('click', noCloseMonParc)
overlayMonParc.addEventListener('click', closePopupMonParc)

function openPopupMonParc() {
    console.log('open')
    overlayMonParc.style.display = 'flex'
}
function noCloseMonParc() {
    noClose = true
}
function closePopupMonParc() {
    if(noClose === true) {
        noClose = false
    } else {
        overlayMonParc.style.display = 'none'
    }
}