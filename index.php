<html>
    <head>
        <title>Cédric Gil - Portfolio</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="fonts/AvenirNext/style.css">
        <link rel="stylesheet" type="text/css" href="css/master.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <?php
        if(isset($_GET['projo'])){
            echo '<link rel="stylesheet" type="text/css" href="css/projo.css">';
        }
        ?>
    </head>
    <body>
        <header>
            <nav id="navbar" class="navbar navbar-expand justify-content-center">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="#">Présentation</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#nav_competences">Compétences</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#nav_projets">Projets</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#nav_veille">Veille</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#nav_contact">Contact</a>
                    </li>
                </ul>
            </nav>
        </header>

        <div class="container-fluid" id="body">

            <div id="presentation">
                <h2>Présentation</h2>
                <div id="card">
                    <div id="card_text">
                        <h3>Cédric GIL</h3>
                        <p>Je m'appelle Cédric Gil, j'ai 19 ans et je suis étudiant en seconde année de BTS SIO SISR (Solutions d'Infrastructures Système et Réseau).<br><br>
                        Diplômé d'un Baccalauréat professionnel SN RISC (Réseaux Informatiques et Systèmes Communicants), j'ai toujours su que l'informatique était mon secteur de prédilection.<br><br>
                        Attiré par l'administration réseau et le développement, j'aime créer des solutions agiles en combinant ces deux domaines.<br><br>
                        Autonome et perfectionniste, j'aime être stimulé par la détermination à résoudre un problème et par l'engouement de mettre sur pieds de nouveaux projets de plus en plus complexes.</p>
                    </div>
                </div>
            </div>

            <div class="line" id="nav_competences">

            </div>

            <div id="competences">
                <h2>Compétences</h2>
                <div id="competences_reseau">
                    <h3>Système & réseau</h3>
                    <div class="list_competences">
                        <div class="competence competence_" id="windows_server">
                            <h4>Windows Server</h4>
                            <img src="img/windows.png">
                            <p><b>- Gestion de parc</b> : DHCP, AD-DS, DNS, WDS, GLPI, FusionInventory (GLPI)<br>
                                <b>- Programmation</b> : Powershell<br>
                                <b>- Serveur WEB</b> : IIS, Wamp<br>
                                <b>- Serveur FTP</b> : FileZilla</p>
                        </div>
                        <div class="competence competence_" id="linux">
                            <h4>Linux</h4>
                            <img src="img/linux.png">
                            <p><b>- Serveur WEB</b> : Apache, MariaDB<br>
                                <b>- Sécurité</b> : Fail2Ban, Squid<br>
                                <b>- Gestion de parc</b> : Zabbix, OpenVPN</p>
                        </div>
                        <div class="competence" id="assistance">
                            <h4>Assistance utilisateur</h4>
                            <img src="img/assistance.png">
                            <p><b>- Gestion de tickets</b> : GLPI</p>
                        </div>
                    </div>
                    <div class="list_competences">
                        <div class="competence" id="architecture">
                            <h4>Architecture réseau</h4>
                            <img src="img/conception.png">
                            <p><b>- Conception</b> : PacketTracer</p>
                        </div>
                        <div class="competence competence_" id="configuration">
                            <h4>Configuration réseau</h4>
                            <img src="img/configuration.png">
                            <p><b>- Routeur PfSense</b> : routage (inter-VLAN), pare-feu, VLSM, NAT, DHCP<br>
                                <b>- Switch Cisco</b> : VLAN, EtherChannel, redondance (STP)<br>
                                <b>- Routeur Cisco</b> : routage (inter-VLAN), VLSM, ACL</p>
                        </div>
                        <div class="competence competence_" id="virtualisation">
                            <h4>Virtualisation</h4>
                            <img src="img/virtualisation.png">
                            <p><b>- Environnements & réseaux virtuels</b> : ESXi (VMware), VirtualBox</p>
                        </div>
                    </div>
                </div>
                <div id="competences_web">
                    <h3>Développement WEB</h3>
                    <div class="list_competences">
                        <div class="competence">
                            <img src="img/html.png">
                            <p>Le langage HTML permet de créer et de structurer des élements dans une page web</p>
                            <div style="background-color: #d2d2d2; border: solid #7c7c7c 1px; border-radius: 5px; width: 80%; margin: 0 auto 15px;">
                                <div style="width: 60%; margin-left: 0; background-color: #ff9700; border-radius: 5px;">
                                    60%
                                </div>
                            </div>
                        </div>
                        <div class="competence">
                            <img src="img/css.svg">
                            <p>Le langage CSS permet de créer le design d'une page internet</p>
                            <div style="background-color: #d2d2d2; border: solid #7c7c7c 1px; border-radius: 5px; width: 80%; margin: 0 auto 15px;">
                                <div style="width: 50%; margin-left: 0; background-color: #ff9700; border-radius: 5px;">
                                    50%
                                </div>
                            </div>
                        </div>
                        <div class="competence">
                            <img src="img/js.png">
                            <p>Le langage JavaScript permet d'animer et de dynamiser les élements d'un site web</p>
                            <div style="background-color: #d2d2d2; border: solid #7c7c7c 1px; border-radius: 5px; width: 80%; margin: 0 auto 15px;">
                                <div style="width: 20%; margin-left: 0; background-color: #ff9700; border-radius: 5px;">
                                    20%
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="list_competences">
                        <div class="competence">
                            <img src="img/php.png">
                            <p>Le langage PHP permet de traiter les données d'un site internet</p>
                            <div style="background-color: #d2d2d2; border: solid #7c7c7c 1px; border-radius: 5px; width: 80%; margin: 0 auto 15px;">
                                <div style="width: 75%; margin-left: 0; background-color: #ff9700; border-radius: 5px;">
                                    75%
                                </div>
                            </div>
                        </div>
                        <div class="competence">
                            <img src="img/phppoo.png">
                            <p>La programmation orientée objet du langage PHP permet de traiter les données d'un site web d'une manière plus complexe, permettant ainsi de créer des fonctionnalités avancées</p>
                            <div style="background-color: #d2d2d2; border: solid #7c7c7c 1px; border-radius: 5px; width: 80%; margin: 0 auto 15px;">
                                <div style="width: 50%; margin-left: 0; background-color: #ff9700; border-radius: 5px;">
                                    50%
                                </div>
                            </div>
                        </div>
                        <div class="competence">
                            <img src="img/sql.png">
                            <p>Le langage SQL permet de communiquer avec un serveur de bases de données comme MySQL, MariaDB...</p>
                            <div style="background-color: #d2d2d2; border: solid #7c7c7c 1px; border-radius: 5px; width: 80%; margin: 0 auto 15px;">
                                <div style="width: 35%; margin-left: 0; background-color: #ff9700; border-radius: 5px;">
                                    35%
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="line" id="nav_projets">

            </div>

            <!-- Projets --> <div>
                <h2>Projets</h2>
                <div class="projets">
                    <h3>Personnels</h3>
                    <div class="projet" id="monparc">
                        <h4>MonParc</h4>
                        <p>Plateforme de tutoriels de gestion de parc informatique</p>
                    </div>
                    <div class="projet" id="myserv">
                        <h4>MyServ</h4>
                        <p>Hébergeur de serveurs de jeu (Minecraft)</p>
                    </div>
                </div>
            </div>

            <div class="line" id="nav_veille">

            </div>

            <div id="veille">
                <h2>Veille technologique</h2>
                <div id="veille_">
                    <h3>L'informatique quantique</h3>
                    <div id="veille_text">
                        <h4>Présentation générale</h4>
                        <p>Un ordinateur comme on le connaît est une machine de Turing qui utilise exclusivement les phénomènes de l’électricité pour fonctionner.<br>
                            Un ordinateur quantique quant à lui fonctionne sur un principe bien plus complexe en utilisant des phénomènes de la mécanique quantique. Il est équipé de composants très spécifiques à sa façon de fonctionner, et très fragiles aux moindres stimuli extérieurs</p>
                        <h5>Comment cela fonctionne plus précisément ?</h5>
                        <p>Revenons rapidement sur le fonctionnement d’un ordinateur classique. Au niveau fondamental, les composants communiquent entre eux et traitent des informations en utilisant simplement la présence ou non d’électricité.<br>
                            Ceci est caractérisé par un Bit ayant pour valeur 0 ou 1. Grossièrement, 0 désigne l’absence d’électricité, et 1 désigne sa présence. C’est l’enchaînement de ces Bits qui va constituer l’information.</p>
                        <p>Un ordinateur quantique fonctionne sur le principe de la superposition quantique, celui-ci ne transmet et ne traite pas l’information avec des simples Bits ayants pour valeur 0 ou 1, mais avec des Q-Bits qui peuvent avoir comme valeur une superposition de l’état 0 et 1 dans différentes proportions.<br>
                            La superposition quantique est un principe fondamental de la matière à l’échèle quantique nous dit qu’une particule élémentaire peut se trouver à la fois dans un état, et en même temps dans un autre. Par exemple, un électron tournant autour du noyau d’un atome se trouve en réalité en même temps dans une position et en même temps dans une autre. En fait, il se trouve partout à la fois autour du noyau en même temps.</p>
                        <img src="img/quantique.jpg">
                        <p>Toujours en utilisant ce principe, une boule peut donc se trouver à l’intérieur ou à l’extérieur d'une boite, ou alors à l’intérieur et à l’extérieur en même temps. La boule est alors dans un état dit superposé.<br>
                            Un Q-Bit peut donc avoir par exemple la valeur 0 à hauteur de 60% et la valeur 1 à hauteur de 40%. Mais en réalité ces proportions correspondent à la probabilité de trouver tel ou tel valeur lors d’une mesure, car cette mesure provoque un autre phénomène appelé la décohérence quantique, qui fait que tout corps ayant un état superposé perd cette propriété lors d’un quelconque acte d’observation. Comme si la particule faisait un tirage au sort de quelle position prendre en fonction des probabilités de celles-ci.</p>
                    </div>
                    <a href="https://1drv.ms/p/s!Ag58xNA0ZxgOj2rIsoXzUckPCPpZ" target="_blank"><button class="popup_link">Présentation PowerPoint</button></a>
                </div>
            </div>

            <div class="line" id="nav_contact">

            </div>

            <div id="contact">
                <h2>Contact</h2>
                <div id="contact_">
                    <h3>Intéressé par mon profil ?</h3>
                    <p>Email : <a href="mailto:cedric.gil@outlook.com">cedric.gil@outlook.com</a><br>
                        Téléphone : (+33)0752025702</p>
                </div>
            </div>

        </div>

        <footer>
            <p><a href="portfolio.zip">Télécharger le code source</a><br>
            Cédric Gil &middot; 2021<br>
            <span id="freepik">Fond : <a href='https://fr.freepik.com/photos/fond'>fr.freepik.com</a></span></p>
        </footer>

        <div class="overlay" id="overlay_windows_server">
            <div class="popup" id="popup_windows_server">
                <h4>Windows Server</h4>
                <h5><u>Les indispensables dans une entreprise :</u></h5>
                <p><b>- DHCP</b> : Protocole servant à attribuer automatiquement une configuration réseau aux périphériques</p>
                <p><b>- AD-DS</b> : Service permettant d'englober un ensemble de périphériques et d'utilisateurs dans un domaine</p>
                <p><b>- DNS</b> : Service de traduction de noms de périphériques en adresse IP</p>
                <p><b>- WDS</b> : Service de déploiement d'images d'installation Windows sur un réseau</p>
                <p><b>- GLPI</b> : Service permettant de créer une plateforme de gestion de tickets, et d'autres fonctionnalités pour gérer un domaine</p>
                <p><b>- FusionInventory (GLPI)</b> : Plugin de GLPI permettant de récupérer les informations matérielles et logicielles d'un ordinateur</p>
                <p><b>- Powershell</b> : Langage de programmation pouvant permettre l'automatisation des tâches Windows</p>
                <h5><u>Création et gestion de serveur WEB :</u></h5>
                <p><b>- IIS</b> : Service WEB incorporé à Windows Server</p>
                <p><b>- WAMP (Windows Apache MySQL PHP)</b> : Plateforme contenant tous les services nécessaires au fonctionnement d'un site internet</p>
                <p><b>- FileZilla Server</b> : Logiciel permettant d'avoir un accès à distance à des fichiers, notamment à ceux d'un site WEB</p>
            </div>
        </div>
        <div class="overlay" id="overlay_linux">
            <div class="popup" id="popup_linux">
                <h4>Linux</h4>
                <h5><u>Les indispensables dans une entreprise :</u></h5>
                <p><b>- Fail2Ban</b> : Service surveillant les tentatives de connexion sur les services présents sur le serveur</p>
                <p><b>- Zabbix</b> : Service qui va récolter les informations matérielles et logicielles d'un ordinateur, ainsi que l'état de fonctionnement de ses services</p>
                <p><b>- Squid</b> : Proxy, facilite et surveille les échanges entre un ordinateur local et un serveur sur internet</p>
                <p><b>- OpenVPN</b> : Serveur VPN qui permet d'avoir un accès distant à un réseau local</p>
                <h5><u>Création de serveur WEB :</u></h5>
                <p><b>- Apache</b> : Service permettant de créer un serveur WEB</p>
                <p><b>- MariaDB</b> : Fork de MySQL servant à créer des bases de données</p>
            </div>
        </div>
        <div class="overlay" id="overlay_configuration">
            <div class="popup" id="popup_configuration">
                <h4>Configuration réseau</h4>
                <h5><u>Les indispensables dans une entreprise :</u></h5>
                <p><b>- Routage</b> : Mécanisme qui permet de faire communiquer des périphériques se trouvant dans des réseaux différents</p>
                <p><b>- Pare-Feu</b> : Service filtrant les connexions entrantes et sortantes</p>
                <p><b>- NAT</b> : Mécanisme de translation d'adresses qui permet de rediriger du trafic</p>
                <p><b>- ACL Cisco</b> : Liste de règles permettant de contrôler le trafic</p>
                <p><b>- VLAN</b> : Réseau local virtuel, permet de segmenter un réseau pour l'optimiser et augmenter sa sécurité</p>
                <h5><u>Pour les plus grandes entreprises :</u></h5>
                <p><b>- EtherChannel</b> : Technologie servant à augmenter la fiabilité des liaisons physiques entres équipements réseau</p>
                <p><b>- VLSM</b> : Technique de découpage d'adresse IP permettant d'optimiser la segmentation d'un réseau</p>
                <p><b>- STP</b> : Protocole qui, maîtrisé, permet de configurer précisément la redondance de commutateurs</p>
            </div>
        </div>
        <div class="overlay" id="overlay_virtualisation">
            <div class="popup" id="popup_virtualisation">
                <h4>Virtualisation</h4>
                <h5><u>Emulation de machines sur poste de travail :</u></h5>
                <p><b>- VirtualBox / VMware Workstation</b> : Logiciels permettant de créer des machines virtuelles dans un environnement de bureau classique</p>
                <h5><u>Emulation de machines sur serveur :</u></h5>
                <p><b>- ESXi (VMware)</b> : Système d'exploitation faisant uniquement office d'hôte pour accueillir des machines virtuelles</p>
            </div>
        </div>
        <div class="overlay" id="overlay_myserv">
            <div class="popup" id="popup_myserv">
                <h4>Compétences mises en &oelig;uvre :</h4>
                <p><b>Windows Server</b> : WAMP, FileZilla, Powershell</p>
                <p><b>Développement WEB</b> : HTML, CSS, JavaScript, PHP (POO), SQL</p>
                <p><b>Configuration réseau</b> : pare-feu, NAT</p>
                <a class="btn_projet" href="http://myserv.tech" target="_blank"><button class="popup_link">MyServ.Tech</button></a>
                <p class="dwn_source"><a href="myserv.zip">Télécharger le code source</a><br>
                    (par sécurité, toutes les informations contenant des adresses IP, des noms d'utilisateurs ou des mots de passe ont été supprimées)</p>
            </div>
        </div>
        <div class="overlay" id="overlay_monparc">
            <div class="popup" id="popup_monparc">
                <h4>En cours de développement</h4>
                <h4>Compétences mises en &oelig;uvre :</h4>
                <p><b>Toutes compétences confondues</b></p>
                <a class="btn_projet" href="http://monparc.cedricgil.fr" target="_blank"><button class="popup_link">MonParc</button></a>
                <p class="dwn_source"><a href="monparc.zip">Télécharger le code source</a><br>
                    (par sécurité, toutes les informations contenant des adresses IP, des noms d'utilisateurs ou des mots de passe ont été supprimées)</p>
            </div>
        </div>

        <div id="overlay_portrait">
            <h2>Tournez votre téléphone en mode paysage</h2>
            <img src="img/portrait.png">
        </div>

        <script src="app/Popup.js"></script>

    </body>
</html>